import { hello, call } from './src/app'

export async function helloHandler(event, context, cb) {
  return hello(event, context, cb)
}

export async function callHandler(event, context, cb) {
  return call(event, context, cb)
}
