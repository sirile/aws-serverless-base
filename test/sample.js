import test from 'ava'
import nock from 'nock'
import ServerlessInvoker from 'serverless-http-invoker'

const invoker = new ServerlessInvoker()
const URL_CALL = 'api/call/v1'
const uuid = 'f511b17d-15f8-4286-b285-a099eab31186'

test('External call', async t => {
  nock('https://httpbin.org').get(`/uuid`).reply(200, { uuid })
  const result = await invoker.invoke(`GET ${URL_CALL}`)
  t.is(result.statusCode, 200)
  t.is(result.body, `Got a result: ${uuid}`)
})
