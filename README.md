# AWS-Serverless-base

Base project of AWS Lambda using the [Serverless](https://serverless.com) framework.

## Features

- ES2019 support through [Babel 7](https://babeljs.io/)
- [WebPack 5](https://webpack.js.org/) used to minimize build size
- [Ava](https://github.com/avajs/ava) for testing
- [nyc](https://github.com/istanbuljs/nyc) and [Istanbul](https://istanbul.js.org/) for coverage
- [ESLint](https://eslint.org/) with [AirBnB-base](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base) for linting
- [Prettier](https://prettier.io/) for code formatting
- [Yarn](https://classic.yarnpkg.com/lang/en/) for package management
- [husky](https://github.com/typicode/husky) with [pretty-quick](https://prettier.io/docs/en/precommit.html) for pre-commit hook
- [serverless-offline](https://github.com/dherault/serverless-offline) for local development
- Support for debugging in VSCode Studio via launch settings

## TODO

- Logging (Winston)
- CI/CD including environment variables for stage and profile etc
- Automatic semantic versioning in GitLab
- Possibly .npmrc to enable private npm-registries

## Things to modify when starting a new function

Some of the following definitions will come from ENV-variables when CI/CD is set up.

- package.json
  - Project name, description etc
- serverless.yml
  - service name
  - default profile

## Basic use cases

### Run locally

```bash
yarn start
```

Test messages can be found from _requests.rest_-file.

### Run tests

```bash
yarn test
```

### Check test coverage

```bash
yarn coverage
open coverage/lcov-report/index.html
```

### Package

```bash
yarn build
```

### Deploy (will also package)

```bash
yarn deploy
```

### Remove

```bash
yarn remove
```

### Debug in debugger

Launch through VSCode Studio debug tab.

### Upgrade packages interactively

```bash
yarn latest
```
