import axios from 'axios'

export async function hello() {
  return {
    statusCode: 200,
    headers: { 'Content-Type': 'text/plain' },
    body: `Hello, world!`,
  }
}

export async function call() {
  // logger.debug('Test logging message')
  const response = await axios(`https://httpbin.org/uuid`)
  return {
    statusCode: 200,
    headers: { 'Content-Type': 'text/plain' },
    body: `Got a result: ${response.data.uuid}`,
  }
}
